from readchar import readkey, key

import file
import generator


def get_length() -> list:
    length_input = input("Enter desired word length - (number) or range (number - number): ")
    length = []

    if length_input.find("-") >= 0:
        while True:
            length.append(int(length_input[:length_input.find("-")]))
            length.append(int(length_input[length_input.find("-") + 1:]))
            if length[0] < length[1]:
                break
            length_input = input("Try a different range (low to high number): ")
            length.clear()
    else:
        length.append(int(length_input))

    return length


def show_config(config: dict):
    print('Current settings:\n'
          '-----------------')
    for name, value in config.items():
        # TODO: Display lengths in a nicer way
        if name == 'length':
            print('{}: {}'.format(name.replace('_', ' ').capitalize(), str(value)))
        else:
            print('{}: {}%'.format(name.replace('_', ' ').capitalize(), str(value * 100)))

    print('')
    show_menu()


def show_menu():
    print("Return - generate new word\n"
          "   (C) - change wordgen config\n"
          "   (L) - list current settings configs\n"
          "   (S) - save results to file\n"
          "   (Q) - exit application\n")


def edit_config(config: dict, config_name: str):
    if config_name == 'length':
        config[config_name] = get_length()
    else:
        config[config_name] = float(input("Enter new value for '{}' (%): ".format(config_name.replace('_', ' ')))) / 100

    print('Setting changed\n')


def config_menu(config: dict):
    config_names = [*config]
    print('Which setting do you wish to amend?:')
    for index, name in enumerate(config_names, start=1):
        print('({}) {}'.format(index, name.replace('_', ' ').capitalize()))
    print('(Q) Back to main menu')
    print('')

    while True:
        choice = readkey()
        if choice.isdigit():
            choice = int(choice)
            if 0 < choice <= len(config_names):
                edit_config(config, config_names[int(choice) - 1])
                break
        elif choice.lower() == 'q':
            break

    show_menu()


def main_menu(grapheme: dict, config: dict):
    print('Welcome to wordgen.py!\n')

    show_config(config)

    while True:
        choice = readkey()
        # 'n' added because key.ENTER doesn't work inside of PyCharm built-in console
        if choice.lower() == 'n' or choice == key.ENTER:
            print(generator.generate_word(grapheme, config))
        elif choice.lower() == 'c':
            config_menu(config)
        elif choice.lower() == 's':
            if len(generator.generated_words) > 0:
                file.save(generator.generated_words)
            else:
                print('No generated words to save!\n')
        elif choice.lower() == 'l':
            show_config(config)
        elif choice.lower() == 'q':
            break
