import file
import menu

def main():

    # INIT SETTINGS - TODO: MAKE CUSTOMISABLE VIA CLI OPTIONS
    config = {
        'length': [10],
        'vowel_start_chance': 0.3,
        'vowel_digraph_chance': 0.05,
        'consonant_digraph_chance': 0.03,
    }

    # load graphemes from files
    grapheme = file.load()

    menu.main_menu(grapheme, config)

if __name__ == "__main__":
    main()
