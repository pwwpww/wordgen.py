#### wordgen.py

---

A tool I wrote to help me with creation of fictional languages.

Designed to produce some tangible results quite quickly, however the project structure allows for easy customisation.

Generation is fairly 'dumb', as it doesn't any fancy word formation theory into account. However, this can also be considered an advantage, because the setting-controllable randomness can create some interesting results. 

#### Run

- `git clone git@gitlab.com:pwwpww/wordgen.py.git && cd wordgen.py`

- `virtualenv -p python3 .env && source .env/bin/activate && pip install -r requirements.txt`

(You should now be inside of the `virtualenv`, if not, run: `source venv/bin/activate`)

- `python wordgen.py`

#### Config

These allow you to change word generation parameters - you can amend them in the CLI menu:
 
- `length` - 1 or 2 (range) integers controlling the generated word length (default: `10`)
- `vowel_start_chance` - chance of generated starting with a vowel (default: `30%`)
- `vowel_digraph_chance` - chance of vowel being a digraph (default: `5%`) 
- `consonant_digraph_chance` - chance of consonant being a digraph (default: `3%`)

#### Questions

>Q: Why are digraphs handled separately?
>
>A: IMO digraphs are very important stylistically in some of my favourite languages (like Finnish, Welsh); I thought that this warrants separate handling for them.

>Q: What about other multigraphs?
>
>A: IMO not frequent enough, but if required, it should be easy enough to add them in!

#### TODO

- add command-line options using `argparse` (changing generation parameters, for example)
- add more validation
- add additional grapheme sources (and a way of selecting them from the menu level)