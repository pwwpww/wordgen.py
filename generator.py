import random
import itertools

generated_words = []


def get_next_grapheme(grapheme: tuple):
    if random.random() < grapheme['digraph_chance']:
        return random.choice(grapheme['double'])
    else:
        return random.choice(grapheme['single'])


def generate_word(grapheme: dict, config: dict) -> str:

    # handles single value/range
    try:
        length = random.randint(config['length'][0], config['length'][1])
    except IndexError:
        length = config['length'][0]

    # select starting grapheme type
    grapheme_types = ['consonant', 'vowel']
    if random.random() < config['vowel_start_chance']:
        grapheme_types.reverse()

    # digraph config -> grapheme
    grapheme['vowel']['digraph_chance'] = config['vowel_digraph_chance']
    grapheme['consonant']['digraph_chance'] = config['consonant_digraph_chance']

    # switch back and forth between vowels and consonants
    grapheme_type = itertools.cycle(grapheme_types)

    # actual generation stuff happens from here onwards
    word = ''

    while len(word) < length:
        next_grapheme_type: str = next(grapheme_type)

        # making sure there's no digraph at the end
        if len(word) + 1 == length:
            grapheme[next_grapheme_type]['digraph_chance'] = 0

        # generate!
        word = word + get_next_grapheme(grapheme[next_grapheme_type])

    generated_words.append(word)

    return word
