def load() -> dict:
    grapheme = {
        'vowel': {},
        'consonant': {},
    }

    with open("graphemes/vowel_single", "r") as f:
        grapheme['vowel']['single'] = tuple(line.rstrip() for line in f)
    with open("graphemes/vowel_double", "r") as f:
        grapheme['vowel']['double'] = tuple(line.rstrip() for line in f)
    with open("graphemes/consonant_single", "r") as f:
        grapheme['consonant']['single'] = tuple(line.rstrip() for line in f)
    with open("graphemes/consonant_double", "r") as f:
        grapheme['consonant']['double'] = tuple(line.rstrip() for line in f)

    return grapheme


def save(generated_words: list):
    filename = input("Enter the file name: ")
    try:
        with open(filename, "w") as file:
            for word in generated_words:
                file.write("%s\n" % word)
        generated_words.clear()
        print("Saved to: {}\n".format(filename))
    except IOError:
        print("Saving failed")



